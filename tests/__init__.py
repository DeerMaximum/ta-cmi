MAKE_GET_REQUEST_PACKAGE = "ta_cmi.api.API._make_request_get"
MAKE_POST_REQUEST_PACKAGE = "ta_cmi.api.API._make_request_post"
DEVICE_DATA_PACKAGE = "ta_cmi.cmi_api.CMIAPI.get_device_data"
COE_DATA_PACKAGE = "ta_cmi.coe_api.CoEAPI.get_coe_data"
COE_VERSION_PACKAGE = "ta_cmi.coe_api.CoEAPI.get_coe_version"
COE_CONFIG_PACKAGE = "ta_cmi.coe_api.CoEAPI.get_coe_server_config"
COE_SEND_DIGITAL_PACKAGE = "ta_cmi.coe_api.CoEAPI.send_digital_values"
COE_SEND_ANALOG_PACKAGE = "ta_cmi.coe_api.CoEAPI.send_analog_values"
COE_SEND_DIGITAL_V2_PACKAGE = "ta_cmi.coe_api.CoEAPI.send_digital_values_v2"
COE_SEND_ANALOG_V2_PACKAGE = "ta_cmi.coe_api.CoEAPI.send_analog_values_v2"
COE_VERSION_CHECK_PACKAGE = "ta_cmi.coe.CoE.check_version"


async def sleep_mock(sleeptime: int) -> None:
    """Mock function to replace asyncio.sleep."""
    pass
